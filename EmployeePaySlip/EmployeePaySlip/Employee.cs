﻿
namespace EmployeePaySlip
{
    public class Employee
    {
        private string firstName;
        private string lastName;
        private double annualSalary;
        private double superRate;
        private string paymentDate;

        private double incomeTax;
        private double superValue;
        private double grossIncome;
        private double netIncome;

        public Employee()
        { 
        }

        public Employee(
            string firstName, 
            string lastName, 
            double annualSalary, 
            double superRate, 
            string paymentDate)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.annualSalary = annualSalary;
            this.superRate = superRate;
            this.paymentDate = paymentDate;
        }
  
        public string FirstName
        {
            get { return firstName;}
            set { firstName = value;}
        }

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        public double AnnualSalary
        {
            get { return annualSalary; }
            set { annualSalary = value; }
        }

        public double SuperRate
        {
            get { return superRate; }
            set { superRate = value; }
        }

        public string PaymentDate
        {
            get { return paymentDate; }
            set { paymentDate = value; }
        }

        public double IncomeTax
        {   get { return incomeTax; }
            set { incomeTax = value; }
        }

        public double SuperValue
        {   get { return superValue; }
            set { superValue = value; }
        }

        public double GrossIncome {
            get { return grossIncome; }
            set { grossIncome = value; }
        }

        public double NetIncome
        {
            get { return netIncome; }
            set { netIncome = value; }
        }

        public string GenerateEmployeeSlip()
        {
            return FirstName + "," + LastName + "," + PaymentDate + "," + GrossIncome + "," + IncomeTax + "," + NetIncome + "," + SuperValue;
        }
    }
}
