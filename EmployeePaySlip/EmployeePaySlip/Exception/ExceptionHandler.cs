﻿using System;

namespace EmployeePaySlip
{
    public class ExceptionHandler : Exception
    {
        private string message;
        public ExceptionHandler(string error) 
            : base(error)
        {
            message = error;    
        }

        public override string Message
        {
            get { return message; }
        }
    }
}
