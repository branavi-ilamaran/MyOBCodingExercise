﻿using System;
using System.Collections.Generic;
using System.IO;
using EmployeePaySlip.Interfaces;

namespace EmployeePaySlip
{
    public class TextFileReader : ITextFileReader
    {
        private string fileName;
        public TextFileReader(string fileName)
        {
            this.fileName = fileName;
        }

        public List<Employee> ReadFile()
        {
            List<Employee> employeeList = new List<Employee>();
            try
            {
                if (string.IsNullOrEmpty(fileName))
                {
                    throw new ExceptionHandler("Input file path is not specified");
                }

                var lines = File.ReadAllLines(fileName, System.Text.Encoding.GetEncoding(1252));
                if (lines.Length == 0)
                {
                    throw new ExceptionHandler("File is empty");
                }

                foreach (var line in lines)
                {
                    string[] employeeDetails = line.Split(',');
                    if (employeeDetails != null && employeeDetails.Length == 5)
                    {
                        Employee employee = new Employee(employeeDetails[0], employeeDetails[1], Double.Parse(employeeDetails[2]), ConvertStringToPercentage(employeeDetails[3]), employeeDetails[4]);
                        employeeList.Add(employee);
                    }
                }
            }
            catch (System.IO.FileNotFoundException ex)
            {
                throw new ExceptionHandler("File '" + fileName + "' not found");
            }
            
            return employeeList;
        }

        public double ConvertStringToPercentage(string value)
        {
            if (!value.EndsWith("%")) 
            {
                throw new ExceptionHandler("Value is not ending with %");
            }

            return Double.Parse(value.Substring(0, value.Length - 1)) / 100;
        }
    }
}
