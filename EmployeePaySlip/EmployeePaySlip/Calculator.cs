﻿using EmployeePaySlip.Interfaces;
using System;
using EmployeePaySlip.Tax.Interfaces;

namespace EmployeePaySlip
{
    public class Calculator : ICalculator
    {
        private Employee employee;
        private ITaxGenerator taxGenerator; 

        public Calculator(ITaxGenerator taxGenerator, Employee employee)
        {
            this.taxGenerator = taxGenerator;
            this.employee = employee;
        }

        public void Calculate()
        {
            if (Validate())
            {
                CalculateGrossIncome();
                CalculateTax();
                CalculateNetIncome();
                CalculateSuper();
            }
        }

        private bool Validate()
        {
            bool isValid = true;
            if (employee == null)
            {
                throw new ExceptionHandler("Invalid Record");
            }

            if (employee.AnnualSalary < 0)
            {
                isValid = false;
                throw new ExceptionHandler("Invalid salary range. Salary value must be a positive integer");
            }
            else if (employee.SuperRate < 0.0 || employee.SuperRate > 0.5)
            {
                isValid = false;
                throw new ExceptionHandler("Super rate must be between 0% - 50% inclusive");
            }

            return isValid;
        }

        private void CalculateGrossIncome()
        {
            employee.GrossIncome = Math.Round(employee.AnnualSalary / 12);
        }

        private void CalculateNetIncome()
        {
            employee.NetIncome = (employee.GrossIncome - employee.IncomeTax);
        }

        private void CalculateSuper()
        {
            employee.SuperValue = Math.Round(employee.GrossIncome * employee.SuperRate);
        }

        private void CalculateTax()
        {
           var tax = taxGenerator.GetTaxData(employee.AnnualSalary);
           if (tax == null)
           {
               throw new ExceptionHandler("Tax is not defined");
           }

           employee.IncomeTax = Math.Round((tax.AdditionalCost + (employee.AnnualSalary - tax.MinLimit) * tax.TaxRate) / 12);
        }
    }
}
