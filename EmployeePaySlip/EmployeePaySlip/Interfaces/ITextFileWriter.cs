﻿
namespace EmployeePaySlip.Interfaces
{
    public interface ITextFileWriter
    {
        void WriteFile();
    }
}
