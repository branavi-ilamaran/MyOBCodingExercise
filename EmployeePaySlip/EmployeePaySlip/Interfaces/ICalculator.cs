﻿
namespace EmployeePaySlip.Interfaces
{
    public interface ICalculator
    {
        void Calculate();
    }
}
