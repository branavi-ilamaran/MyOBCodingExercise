﻿using System.Collections.Generic;

namespace EmployeePaySlip.Interfaces
{
    public interface ITextFileReader
    {
        List<Employee> ReadFile();

        double ConvertStringToPercentage(string value);
    }
}
