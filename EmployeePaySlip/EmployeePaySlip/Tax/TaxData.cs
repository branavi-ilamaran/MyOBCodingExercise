﻿
namespace EmployeePaySlip.Tax
{
    public class TaxData
    {
        public class Tax1
        {
            public const int MinLimit = 0;
            public const int MaxLimit = 18200;
            public const double TaxRate = 0;
            public const double AdditionalCost = 0;
        }

        public class Tax2
        {
            public const int MinLimit = 18201;
            public const int MaxLimit = 37000;
            public const double TaxRate = 0.19;
            public const double AdditionalCost = 0;
        }

        public class Tax3
        {
            public const int MinLimit = 37001;
            public const int MaxLimit = 80000;
            public const double TaxRate = 0.325;
            public const double AdditionalCost = 3572;
        }

        public class Tax4
        {
            public const int MinLimit = 80001;
            public const int MaxLimit = 180000;
            public const double TaxRate = 0.37;
            public const double AdditionalCost = 17547;
        }

        public class Tax5
        {
            public const int MinLimit = 180001;
            public const int MaxLimit = int.MaxValue;
            public const double TaxRate = 0.45;
            public const double AdditionalCost = 54547;
        }
    }
}
