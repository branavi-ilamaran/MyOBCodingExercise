﻿using EmployeePaySlip.Tax.Interfaces;
using System.Collections.Generic;
using System.Linq;
using TaxData1 = EmployeePaySlip.Tax.TaxData.Tax1;
using TaxData2 = EmployeePaySlip.Tax.TaxData.Tax2;
using TaxData3 = EmployeePaySlip.Tax.TaxData.Tax3;
using TaxData4 = EmployeePaySlip.Tax.TaxData.Tax4;
using TaxData5 = EmployeePaySlip.Tax.TaxData.Tax5;

namespace EmployeePaySlip.Tax
{
    public class TaxGenerator : ITaxGenerator
    {
        private readonly List<Tax> taxes = new List<Tax>();

        public TaxGenerator()
        {
            taxes.Add(new Tax(TaxData1.MinLimit, TaxData1.MaxLimit, TaxData1.TaxRate, TaxData1.AdditionalCost));
            taxes.Add(new Tax(TaxData2.MinLimit, TaxData2.MaxLimit, TaxData2.TaxRate, TaxData2.AdditionalCost));
            taxes.Add(new Tax(TaxData3.MinLimit, TaxData3.MaxLimit, TaxData3.TaxRate, TaxData3.AdditionalCost));
            taxes.Add(new Tax(TaxData4.MinLimit, TaxData4.MaxLimit, TaxData4.TaxRate, TaxData4.AdditionalCost));
            taxes.Add(new Tax(TaxData5.MinLimit, TaxData5.MaxLimit, TaxData5.TaxRate, TaxData5.AdditionalCost));
        }

        public Tax GetTaxData(double annualSalary)
        {
            return taxes.SingleOrDefault(t => annualSalary >= t.MinLimit && annualSalary <= t.MaxLimit);
        }
    }
}
