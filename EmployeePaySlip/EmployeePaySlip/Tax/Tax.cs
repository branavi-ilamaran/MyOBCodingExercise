﻿
namespace EmployeePaySlip.Tax
{
    public class Tax
    {
        private int maxLimit;
        private int minLimit;
        private double taxRate;
        private double additionalCost;

        public Tax(int minLimit, int maxLimit, double taxRate, double additionalCost)
        {
            this.minLimit = minLimit;
            this.maxLimit = maxLimit;
            this.taxRate = taxRate;
            this.additionalCost = additionalCost;
        }

        public int MaxLimit
        { 
            get { return maxLimit; }
            set { maxLimit = value; }
        }

        public int MinLimit
        {
            get { return minLimit; }
            set { minLimit = value; }
        }

        public double TaxRate
        {
            get { return taxRate; }
            set { taxRate = value; }
        }

        public double AdditionalCost
        {
            get { return additionalCost; }
            set { additionalCost = value; }
        }
    }
}
