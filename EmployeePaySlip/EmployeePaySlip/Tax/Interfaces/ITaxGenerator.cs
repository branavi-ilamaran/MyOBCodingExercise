﻿
namespace EmployeePaySlip.Tax.Interfaces
{
    public interface ITaxGenerator
    {
        Tax GetTaxData(double annualSalary);
    }
}
