﻿using System;
using System.Collections.Generic;
using System.IO;
using EmployeePaySlip.Interfaces;

namespace EmployeePaySlip
{
    public class TextFileWriter : ITextFileWriter
    {
        private List<Employee> employees = new List<Employee>();
        private string fileName;

        public TextFileWriter(string fileName, List<Employee> employees)
        {
            this.fileName = fileName;
            this.employees = employees;
        }

        public void WriteFile()
        {
            try
            {
                if (string.IsNullOrEmpty(fileName))
                {
                    throw new ExceptionHandler("Output file path is not specified");
                }

                using (StreamWriter file =
                     new StreamWriter(fileName))
                {
                    foreach (Employee employee in employees)
                    {
                        if (employee != null)
                        {
                            file.WriteLine(employee.GenerateEmployeeSlip());
                        }
                    }
                }
            }
            catch (IOException ex)
            {
                throw new ExceptionHandler("Unable to write");
            }

            Console.WriteLine("Payslip has been successfully generated");
        }
    }
}
