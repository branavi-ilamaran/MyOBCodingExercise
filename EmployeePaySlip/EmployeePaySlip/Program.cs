﻿using EmployeePaySlip.Interfaces;
using EmployeePaySlip.Tax;
using EmployeePaySlip.Tax.Interfaces;
using System;
using System.Collections.Generic;
using Ninject;
using System.Reflection;
using System.Configuration;

namespace EmployeePaySlip
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var inputFile = ConfigurationManager.AppSettings["InputFile"];
            var outputFile = ConfigurationManager.AppSettings["OutputFile"];
            try
            {
                Console.WriteLine("*********Employee PaySlip Generation*********");
                Console.WriteLine("Input file:" + inputFile);
                Console.WriteLine("Output file:" + outputFile);

                StandardKernel _Kernal = new StandardKernel();
                _Kernal.Load(Assembly.GetExecutingAssembly());
                ITaxGenerator objTaxCalculator = _Kernal.Get<ITaxGenerator>();

                Execute(inputFile, outputFile, objTaxCalculator);
            }
            catch (ExceptionHandler ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();
        }

        private static void Execute(string inputFile, string outputFile, ITaxGenerator objTaxCalculator)
        {
            TextFileReader textFileReader = new TextFileReader(inputFile);
            List<Employee> employees = textFileReader.ReadFile();

            foreach (Employee employee in employees)
            {
                Calculator calculator = new Calculator(objTaxCalculator, employee);
                calculator.Calculate();
            }

            TextFileWriter textFileWriter = new TextFileWriter(outputFile, employees);
            textFileWriter.WriteFile();
        }
    }

    public class EmployeePaySlipeModule : Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            Bind<ICalculator>().To<Calculator>();
            Bind<ITextFileReader>().To<TextFileReader>();
            Bind<ITextFileWriter>().To<TextFileWriter>();
            Bind<ITaxGenerator>().To<TaxGenerator>();
        } 
    }
}
