﻿using System.Collections.Generic;
using NUnit.Framework;

namespace EmployeePaySlip.Test
{
    [TestFixture]
    public class TextFileReaderTest
    {
        private TextFileReader textFileReader;
        private string validFilePath = @"../../EmployeeDetails.txt";
        private ExceptionHandler exceptionHandler;

        [SetUp]
        public void Setup()
        {
            textFileReader = new TextFileReader(validFilePath);
        }

        [Test]
        public void TestReadInvalidFile()
        {
            TextFileReader textInValidFileReader = new TextFileReader("black.txt");
            exceptionHandler = ExceptionAssert.Throws<ExceptionHandler>(() => textInValidFileReader.ReadFile());
            Assert.AreEqual("File 'black.txt' not found", exceptionHandler.Message);
        }

        [Test]
        public void TestPercentageToDoubleWithInvalidSuperRate()
        {
            exceptionHandler = ExceptionAssert.Throws<ExceptionHandler>(()=> textFileReader.ConvertStringToPercentage("1"));
            Assert.AreEqual("Value is not ending with %", exceptionHandler.Message);

        }

        [Test]
        public void TestEmptyFilePath()
        {
            TextFileReader textInValidFileReader = new TextFileReader("");
            exceptionHandler = ExceptionAssert.Throws<ExceptionHandler>(() => textInValidFileReader.ReadFile());
            Assert.AreEqual("Input file path is not specified", exceptionHandler.Message);
        }

        [Test]
        public void TestPercentageToDoubleWithValidSuperRate()
        {
            textFileReader.ConvertStringToPercentage("1%");
            Assert.AreEqual(1, 1);
        }

        [Test]
        public void TestReadValidFile()
        {
            List<Employee> employees = textFileReader.ReadFile();
            Assert.IsNotNull(employees);
            Assert.AreEqual(2, employees.Count);

            Employee employee1 = employees[0];
            Assert.AreEqual("David", employee1.FirstName);
            Assert.AreEqual("Rudd", employee1.LastName);
            Assert.AreEqual("01 March – 31 March", employee1.PaymentDate);
            Assert.AreEqual(60050, employee1.AnnualSalary);
            Assert.AreEqual(0.09, employee1.SuperRate);

            Employee employee2 = employees[1];
            Assert.AreEqual("Ryan", employee2.FirstName);
            Assert.AreEqual("Chen", employee2.LastName);
            Assert.AreEqual("01 March – 31 March", employee2.PaymentDate);
            Assert.AreEqual(120000, employee2.AnnualSalary);
            Assert.AreEqual(0.1, employee2.SuperRate);
        }
    }
}
