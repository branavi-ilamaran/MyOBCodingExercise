﻿using NUnit.Framework;
using EmployeePaySlip.Interfaces;
using EmployeePaySlip.Tax.Interfaces;
using Moq;

namespace EmployeePaySlip.Test
{
    [TestFixture]
    public  class CalculatorTest
    {
        private Employee employee = new Employee()
        {
            FirstName = "David",
            LastName = "Rudd",
            PaymentDate = "01 March - 31 March",
            SuperRate = 0.09,
            AnnualSalary = 60050
        };

        private Mock<ITaxGenerator> taxGenerator;
        private ICalculator calculator;
        private ExceptionHandler exceptionHandler;

        [SetUp]
        public void Setup()
        {
           taxGenerator = new Mock<ITaxGenerator>();
           taxGenerator.Setup(t => t.GetTaxData(It.IsAny<double>())).Returns(new Tax.Tax(37001, 80000, 0.325, 3572));
           calculator = new Calculator(taxGenerator.Object, employee);
        }

        [Test]
        public void TestCalculateEmployeeDetails()
        {
            calculator.Calculate();
            Assert.AreEqual(5004, employee.GrossIncome);
            Assert.AreEqual(922, employee.IncomeTax);
            Assert.AreEqual(4082, employee.NetIncome);
            Assert.AreEqual(450, employee.SuperValue);
        }

        [Test]
        public void TestIfEmployeeNotExist()
        {
            calculator = new Calculator(taxGenerator.Object, null);
            exceptionHandler = ExceptionAssert.Throws<ExceptionHandler>(() => calculator.Calculate());
            Assert.AreEqual("Invalid Record", exceptionHandler.Message);
        }

        [Test]
        public void TestIfAnnualSalaryIsNegative()
        {
            Employee employeeWithInvalidSalary = new Employee() { AnnualSalary = -1 };
            calculator = new Calculator(taxGenerator.Object, employeeWithInvalidSalary);
            exceptionHandler = ExceptionAssert.Throws<ExceptionHandler>(() => calculator.Calculate());
            Assert.AreEqual("Invalid salary range. Salary value must be a positive integer", exceptionHandler.Message);
        }

        [Test]
        public void TestIfSuperRateIsNegative()
        {
            Employee employeeWithInvalidSuperRate = new Employee() { SuperRate = -1 };
            calculator = new Calculator(taxGenerator.Object, employeeWithInvalidSuperRate);
            exceptionHandler = ExceptionAssert.Throws<ExceptionHandler>(() => calculator.Calculate());
            Assert.AreEqual("Super rate must be between 0% - 50% inclusive", exceptionHandler.Message);
        }

        [Test]
        public void TestIfSuperRateIsOutOfPostiveRange()
        {
            Employee employeeWithInvalidSuperRate = new Employee() { SuperRate = 10 };
            calculator = new Calculator(taxGenerator.Object, employeeWithInvalidSuperRate);
            exceptionHandler = ExceptionAssert.Throws<ExceptionHandler>(() => calculator.Calculate());
            Assert.AreEqual("Super rate must be between 0% - 50% inclusive", exceptionHandler.Message);
        }
    }
}
