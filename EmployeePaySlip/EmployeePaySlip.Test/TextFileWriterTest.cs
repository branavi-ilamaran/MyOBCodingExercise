﻿using System.Collections.Generic;
using NUnit.Framework;

namespace EmployeePaySlip.Test
{
    [TestFixture]
    public class TextFileWriterTest
    {
        private TextFileWriter textFileWriter;
        private string validFilePath = @"../../EmployeePaySlip.txt";
        private ExceptionHandler exceptionHandler;

        private Employee employee1 = new Employee()
        {
            FirstName = "David",
            LastName = "Rudd",
            PaymentDate = "01 March - 31 March",
            GrossIncome = 5004,
            IncomeTax = 922,
            NetIncome = 4082,
            SuperValue = 450
        };

        [SetUp]
        public void Setup()
        {
            textFileWriter = new TextFileWriter(validFilePath, new List<Employee>(){employee1});
        }

        [Test]
        public void TestWithInvalidFilePath()
        {
            textFileWriter = new TextFileWriter("", new List<Employee>() { employee1 });
            exceptionHandler = ExceptionAssert.Throws<ExceptionHandler>(() => textFileWriter.WriteFile());
            Assert.AreEqual("Output file path is not specified", exceptionHandler.Message);
        }
    }
}
