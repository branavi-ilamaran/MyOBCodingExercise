﻿using NUnit.Framework;
using EmployeePaySlip.Tax;

namespace EmployeePaySlip.Test
{
    [TestFixture]
    public class TaxGeneratorTest
    {
        private TaxGenerator taxGenerator;

        [SetUp]
        public void Setup()
        {
            taxGenerator = new TaxGenerator();
        }

        [Test]
        public void TestGetValidTaxData()
        {
            var tax = taxGenerator.GetTaxData(37006);
            Assert.IsNotNull(tax);
            Assert.AreEqual(0.325, tax.TaxRate);
            Assert.AreEqual(3572, tax.AdditionalCost);
        }

        [Test]
        public void TestGetInValidTaxData()
        {
            var tax = taxGenerator.GetTaxData(-1);
            Assert.IsNull(tax);
        }
    }
}
